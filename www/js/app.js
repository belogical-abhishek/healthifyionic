// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', ['ionic', 'starter.controllers','pascalprecht.translate', 'starter.services', "ngStorage"])

.run(function($ionicPlatform, $translate) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if(typeof navigator.globalization !== "undefined") {
      navigator.globalization.getPreferredLanguage(function(language) {
        $translate.use((language.value).split("-")[0]).then(function(data) {
          console.log("Success : " + data);
        }, function(error) {
          console.log("Failed : " + data);
        });
      }, null);
    }
    
    document.addEventListener('deviceready', function() {
      navigator.splashscreen.hide();
    });    
  });
})

.config(function($stateProvider, $urlRouterProvider, $translateProvider) {
  $translateProvider.translations("en", {
    JOIN_NOW_MESSAGE: "Join now",
    SIGN_IN_MESSAGE: "Sign in",
    PASSWORD_REQUIRED_MESSAGE: "Password is required.",
    EMAIL_ID_REQUIRED_MESSAGE: "Email-Id is required.",
    INVALID_EMAIL_ID_ERROR_MESSAGE: "Invalid Email-Id",
    LOGIN_MESSAGE: "Login",
    ENTER_EMAIL_ID_MESSAGE:"Enter your Email-ID",
    ENTER_PASSWORD_MESSAGE: "Password",

    BOOK_APPOINTMENT_MESSAGE:"Book Appointment",
    PATIENT_TITLE_MESSAGE: "Patient",
    APPOINTMENT_TITLE_MESSAGE: "Appointment",
    FINISHED_TITLE_MESSAGE: "Finished",
    PATIENT_INFORMATION_TITLE_MESSAGE: "Patient Information",
    USED_HEALTHIFY_TITLE_MESSAGE: "Have you used Healthify before?",
    NEW_TO_HEALTHIFY_MESSAGE: "Yes, I'm New to Healthify",
    HAVE_USED_HEALTHIFY_MESSAGE: "I've used Healthify before",
    PATIENT_NAME_TITLE_MESSAGE: "Patient name",
    GENDER_TITLE_MESSAGE: "Gender",
    GENDER_NOT_SPECIFIED_MESSAGE: "Not Specified",
    GENDER_MALE_MESSAGE: "Male",
    GENDER_FEMALE_MESSAGE: "Female",
    APPOINTMENT_TIME_TITLE_MESSAGE: "Appointment Time",
    INSURANCE_QUESTION_MESSAGE: "Will you use insurance ?",
    REASON_OF_VISIT_QUESTION_MESSAGE: "What is the reason for your visit?",
    VISITED_DOCTOR_BEFORE_QUESTION_MESSAGE: "Have you visited this doctor before?",
    NEW_PATIENT_MESSAGE: "I am a new Patient",
    VISITED_DOCTOR_BEFORE_MESSAGE: "I have visited this doctor before",
    BOOKING_COMPLETE_MESSAGE: "BOOKING COMPLETED",
    BOOKING_CONFIRMATION_MESSAGE: "Your booking has been confirmed.",
    THANK_YOU_MESSAGE: "Thank you.",
    MY_BOOKING_MESSAGE: "My Booking",
    HOME_TITLE_MESSAGE: "Home",

    RESET_PASSWORD_TITLE_MESSAGE: "Reset Password",
    PROCEED_MESSAGE: "Proceed",

    SEARCH_DOCTOR_CLINICS_MESSAGE: "Search doctor and clinics",
    SPECIALITY_MESSAGE: "Speciality",
    LOCATION_MESSAGE: "Location",
    DATE_MESSAGE: "Date",
    INSURANCE_MESSAGE: "Insurance",
    FIND_DOCTOR_MESSAGE: "Find Doctor",
  
    BLOCKED_CONFIGURATION_MESSAGE: "Sorry..! You are blocked please check your configuration.",

    SELECT_DATE_MESSAGE: "SELECT DATE",
    DONE_MESSAGE: "Done",

    SELECT_SPECIALITY_MESSAGE: "Select a Speciality",

    MENU_MESSAGE: "MENU",
    HOME_MESSAGE: "Home",
    LOGIN_MESSAGE: "Login",
    SIGN_UP_MESSAGE: "Sign Up",
    MY_BOOKING_MESSAGE: "My Booking",
    LOGOUT_MESSAGE: "Logout",

    SEARCH_RESULT_MESSAGE: "Search Result",
    LAST_BOOKING_SESSION_MESSAGE: "Last Booking Session",
    REASON_MESSAGE: "Reason",
    TIME_MESSAGE: "Time",
    LAST_ONE_STATUS_MESSAGE: "Last one status",
    APPROVED_MESSAGE: "Approved",
    CANCELLED_MESSAGE: "Cancelled",
    PENDING_MESSAGE: "Pending",
    VIEW_DOCTOR_MESSAGE: "View Doctor",
    NO_RESULTS_MESSAGE: "No Results",

    SIGN_UP_MESSAGE: "SignUp",
    FIRST_NAME_MESSAGE: "First Name",
    FIRST_NAME_REQUIRED_MESSAGE: "First Name is required.",
    LAST_NAME_MESSAGE: "Last Name",
    LAST_NAME_REQUIRED_MESSAGE: "Last Name is required.",
    EMAIL_MESSAGE: "Email",
    PASSWORD_MINIMUM_CHARS_MESSAGE: "Password should have minimum 6 characters.",
    ENTER_MOBILE_MESSAGE: "Mobile",
    MOBILE_NUMBER_REQUIRED_MESSAGE: "Mobile Number is required.",
    MOBILE_NUMBER_LIMIT_MESSAGE: "Mobile Number should be 10 digits.",
    AGREE_CONDITIONS_MESSAGE: "I agree with Terms and Conditions",
    MUST_AGREE_CONDITIONS_MESSAGE: "Must agree the Terms and Conditions.",

    LANGUAGE_OPTIONS_MESSAGE: "Language Option",
    SETTINGS_MESSAGE: "Settings",
    APPOINTMENT_MESSAGE: "Appointment",
    BOOK_AGAIN_MESSAGE: "Book Again"
  });

  $translateProvider.translations("yo", {
    JOIN_NOW_MESSAGE: "Dapọ nisisiyi",
    SIGN_IN_MESSAGE: "wọle",
    PASSWORD_REQUIRED_MESSAGE: "Ti beere fun ọrọigbaniwọle.",
    EMAIL_ID_REQUIRED_MESSAGE: "Imeeli-Id ni a nilo.",
    INVALID_EMAIL_ID_ERROR_MESSAGE: "Imeeli-Id ni a nilo.",
    LOGIN_MESSAGE: "Wo ile",
    ENTER_EMAIL_ID_MESSAGE:"Tẹ ID Imeeli rẹ sii",
    ENTER_PASSWORD_MESSAGE: "Ọrọigbaniwọle",

    BOOK_APPOINTMENT_MESSAGE:"Ipade Ilana",
    PATIENT_TITLE_MESSAGE: "Ipade Ilana",
    APPOINTMENT_TITLE_MESSAGE: "Ijoba",
    FINISHED_TITLE_MESSAGE: "Ti pari",
    PATIENT_INFORMATION_TITLE_MESSAGE: "Alaye Alaisan",
    USED_HEALTHIFY_TITLE_MESSAGE: "Ṣe o ti lo Healthify ṣaaju ki o to?",
    NEW_TO_HEALTHIFY_MESSAGE: "Bẹẹni, Mo wa New si Healthify",
    HAVE_USED_HEALTHIFY_MESSAGE: "Mo ti lo Healthify ṣaaju ki o to",
    PATIENT_NAME_TITLE_MESSAGE: "Orukọ alaisan",
    GENDER_TITLE_MESSAGE: "Iwa",
    GENDER_NOT_SPECIFIED_MESSAGE: "Lai so ni pato",
    GENDER_MALE_MESSAGE: "Okunrin",
    GENDER_FEMALE_MESSAGE: "Obinrin",
    APPOINTMENT_TIME_TITLE_MESSAGE: "Akoko akoko ipe",
    INSURANCE_QUESTION_MESSAGE: "Ṣe iwọ yoo lo iṣeduro?",
    REASON_OF_VISIT_QUESTION_MESSAGE: "Kini idi fun ibewo rẹ?",
    VISITED_DOCTOR_BEFORE_QUESTION_MESSAGE: "Ṣe o ti ṣàbẹwò dọkita yii ṣaaju ki o to?",
    NEW_PATIENT_MESSAGE: "Mo jẹ Alaisan titun kan",
    VISITED_DOCTOR_BEFORE_MESSAGE: "Mo ti ṣàbẹwò si dokita yii ṣaaju ki o to",
    BOOKING_COMPLETE_MESSAGE: "IKỌ NIPẸ Pari",
    BOOKING_CONFIRMATION_MESSAGE: "O ti fi idiwe rẹ silẹ.",
    THANK_YOU_MESSAGE: "E dupe.",
    MY_BOOKING_MESSAGE: "Atunwo mi",
    HOME_TITLE_MESSAGE: "Ile",

    RESET_PASSWORD_TITLE_MESSAGE: "Atunwo Ọrọigbaniwọle",
    PROCEED_MESSAGE: "Tẹsiwaju",

    SEARCH_DOCTOR_CLINICS_MESSAGE: "Wa dokita ati ile iwosan",
    SPECIALITY_MESSAGE: "Okan nigboro",
    LOCATION_MESSAGE: "Ipo",
    DATE_MESSAGE: "Ọjọ",
    INSURANCE_MESSAGE: "Iṣeduro",
    FIND_DOCTOR_MESSAGE: "Wa Dokita",
  
    BLOCKED_CONFIGURATION_MESSAGE: "Binu ..! O ti dina mọwo ṣayẹwo iṣeto rẹ.",

    SELECT_DATE_MESSAGE: "ṢẸ DATE",
    DONE_MESSAGE: "Ṣe",

    SELECT_SPECIALITY_MESSAGE: "Yan Okan nigboro",

    MENU_MESSAGE: "MENU",
    HOME_MESSAGE: "Ile",
    LOGIN_MESSAGE: "Wo ile",
    SIGN_UP_MESSAGE: "Forukọsilẹ",
    LOGOUT_MESSAGE: "Logout",

    SEARCH_RESULT_MESSAGE: "Esi Iwadi",
    LAST_BOOKING_SESSION_MESSAGE: "Ikẹkọ Ikilọ Ikẹhin",
    REASON_MESSAGE: "Idi",
    TIME_MESSAGE: "Aago",
    LAST_ONE_STATUS_MESSAGE: "Ipo ti o kẹhin",
    APPROVED_MESSAGE: "Ti fọwọsi",
    CANCELLED_MESSAGE: "Ti fagile",
    PENDING_MESSAGE: "Ni isunmọtosi",
    VIEW_DOCTOR_MESSAGE: "Wo Dokita",
    NO_RESULTS_MESSAGE: "Ko si esi",

    SIGN_UP_MESSAGE: "Forukọsilẹ",
    FIRST_NAME_MESSAGE: "Orukọ kini",
    FIRST_NAME_REQUIRED_MESSAGE: "A beere orukọ akọkọ.",
    LAST_NAME_MESSAGE: "Oruko idile",
    LAST_NAME_REQUIRED_MESSAGE: "Orukọ idile ni a beere.",
    EMAIL_MESSAGE: "Imeeli",
    PASSWORD_MINIMUM_CHARS_MESSAGE: "Ọrọigbaniwọle yẹ ki o ni awọn lẹta ti o kere ju 6.",
    ENTER_MOBILE_MESSAGE: "Mobile",
    MOBILE_NUMBER_REQUIRED_MESSAGE: "Nọmba Mo ti beere.",
    MOBILE_NUMBER_LIMIT_MESSAGE: "Nọmba Mobile gbọdọ jẹ awọn nọmba 10.",
    AGREE_CONDITIONS_MESSAGE: "Mo gba pẹlu Awọn ofin ati ipo",
    MUST_AGREE_CONDITIONS_MESSAGE: "Gbọdọ gba awọn ofin ati ipo.",
  
    LANGUAGE_OPTIONS_MESSAGE: "Aṣayan Ede",
    SETTINGS_MESSAGE: "Ètò",
    APPOINTMENT_MESSAGE: "Ijoba",
    BOOK_AGAIN_MESSAGE: "Iwe tun"
  });

  $translateProvider.translations("ha", {
    JOIN_NOW_MESSAGE: "Shiga yanzu",
    SIGN_IN_MESSAGE: "Shiga",
    PASSWORD_REQUIRED_MESSAGE: "Ana buƙatar kalmar wucewa.",
    EMAIL_ID_REQUIRED_MESSAGE: "Ana buƙatar Email-Id.",
    INVALID_EMAIL_ID_ERROR_MESSAGE: "Email-Id mara inganci"    ,
    LOGIN_MESSAGE: "Shiga",
    ENTER_EMAIL_ID_MESSAGE:"Shigar da adireshin Imel ɗin ku",
    ENTER_PASSWORD_MESSAGE: "Kalmar sirri",

    BOOK_APPOINTMENT_MESSAGE:"Ƙayyadar Littafin",
    PATIENT_TITLE_MESSAGE: "Mai haƙuri",
    APPOINTMENT_TITLE_MESSAGE: "Ƙayyadewa",
    FINISHED_TITLE_MESSAGE: "An gama",
    PATIENT_INFORMATION_TITLE_MESSAGE: "Bayanin haƙuri",
    USED_HEALTHIFY_TITLE_MESSAGE: "Shin kun yi amfani da Healthify kafin?",
    NEW_TO_HEALTHIFY_MESSAGE: "Haka ne, Ni New to Healthify",
    HAVE_USED_HEALTHIFY_MESSAGE: "Na yi amfani da Healthify kafin",
    PATIENT_NAME_TITLE_MESSAGE: "Sunan haƙuri",
    GENDER_TITLE_MESSAGE: "Gender",
    GENDER_NOT_SPECIFIED_MESSAGE: "Ba a kayyade ba",
    GENDER_MALE_MESSAGE: "Mace",
    GENDER_FEMALE_MESSAGE: "Tamace",
    APPOINTMENT_TIME_TITLE_MESSAGE: "Lokacin hajji",
    INSURANCE_QUESTION_MESSAGE: "Kuna amfani da inshora?",
    REASON_OF_VISIT_QUESTION_MESSAGE: "Mene ne dalilin da kuka ziyarta?",
    VISITED_DOCTOR_BEFORE_QUESTION_MESSAGE: "Kun ziyarci wannan likita kafin?",
    NEW_PATIENT_MESSAGE: "Ni sabon Sahun",
    VISITED_DOCTOR_BEFORE_MESSAGE: "Na ziyarci wannan likita kafin",
    BOOKING_COMPLETE_MESSAGE: "LITTAFI KASHE",
    BOOKING_CONFIRMATION_MESSAGE: "An tabbatar da littafinku.",
    THANK_YOU_MESSAGE: "Na gode.",
    MY_BOOKING_MESSAGE: "Mene Littafi",
    HOME_TITLE_MESSAGE: "Gida",

    RESET_PASSWORD_TITLE_MESSAGE: "Sake saita kalmar sirri",
    PROCEED_MESSAGE: "Ci gaba",

    SEARCH_DOCTOR_CLINICS_MESSAGE: "Bincika likita da dakunan shan magani",
    SPECIALITY_MESSAGE: "Musamman",
    LOCATION_MESSAGE: "Yanayi",
    DATE_MESSAGE: "Rana",
    INSURANCE_MESSAGE: "Inshuwara",
    FIND_DOCTOR_MESSAGE: "Nemo Doctor",
  
    BLOCKED_CONFIGURATION_MESSAGE: "Yi haƙuri ..! An katange don Allah a duba tsarinka.",

    SELECT_DATE_MESSAGE: "KASHE DATE",
    DONE_MESSAGE: "Anyi",

    SELECT_SPECIALITY_MESSAGE: "Zaɓi Musamman",

    MENU_MESSAGE: "MENU",
    HOME_MESSAGE: "Gida",
    LOGIN_MESSAGE: "Shiga",
    SIGN_UP_MESSAGE: "Sa hannu",
    LOGOUT_MESSAGE: "Binciken",

    SEARCH_RESULT_MESSAGE: "Sakamakon Bincike",
    LAST_BOOKING_SESSION_MESSAGE: "Zama na Farko",
    REASON_MESSAGE: "Dalili",
    TIME_MESSAGE: "Lokaci",
    LAST_ONE_STATUS_MESSAGE: "Matsayi na karshe",
    APPROVED_MESSAGE: "Tabbas",
    CANCELLED_MESSAGE: "An soke",
    PENDING_MESSAGE: "Ana jiran",
    VIEW_DOCTOR_MESSAGE: "Duba Doctor",
    NO_RESULTS_MESSAGE: "Babu Sakamako",

    FIRST_NAME_MESSAGE: "Sunan rana",
    FIRST_NAME_REQUIRED_MESSAGE: "Ana buƙatar sunan farko.",
    LAST_NAME_MESSAGE: "Sunan mahaifa",
    LAST_NAME_REQUIRED_MESSAGE: "Ana buƙatar suna na ƙarshe.",
    EMAIL_MESSAGE: "Imel",
    PASSWORD_MINIMUM_CHARS_MESSAGE: "Dole ne kalmar sirri ta kasance da haruffa 6.",
    ENTER_MOBILE_MESSAGE: "Mobile",
    MOBILE_NUMBER_REQUIRED_MESSAGE: "Lambar Wayar da ake bukata.",
    MOBILE_NUMBER_LIMIT_MESSAGE: "Lambar Wayar ya zama lambobi 10.",
    AGREE_CONDITIONS_MESSAGE: "Na yarda da Yarjejeniyar da Yanayin",
    MUST_AGREE_CONDITIONS_MESSAGE: "Dole ne ya yarda da Dokokin da Yanayi.",

    LANGUAGE_OPTIONS_MESSAGE: "Harshe Harshe",
    SETTINGS_MESSAGE: "Saituna",
    APPOINTMENT_MESSAGE: "Ƙayyadewa",
    BOOK_AGAIN_MESSAGE: "Littafin sake"
  });

  $translateProvider.preferredLanguage("en");
  $translateProvider.fallbackLanguage("en");

  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
.state('app.bmd-home', {
  cache: false,
      url: '/bmd-home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'BMDCtrl'
        }
      }
    })
.state('app.bmd-blocked', {
  cache: false,
      url: '/bmd-blocked',
      views: {
        'menuContent': {
          templateUrl: 'templates/blocked.html',
          controller: 'BMDCtrl'
        }
      }
    })
.state('app.listing', {
  cache: false,
      url: '/listing/:selectionId',
      views: {
        'menuContent': {
          templateUrl: 'templates/listing.html',
          controller: 'BMDCtrl'
        }
      }
    })
.state('app.search', {
  cache: false,
      url: '/search',
      views: {
        'menuContent': {
          templateUrl: 'templates/search.html',
          controller: 'searchCtrl'
        }
      }
    })
.state('app.doctor', {
  cache: false,
      url: '/doctor',
      views: {
        'menuContent': {
          templateUrl: 'templates/search_result.html',
          controller: 'searchCtrl'
        }
      }
    })
.state('app.appointment', {
  cache: false,
      url: '/appointment/:apnt_date_time',
      views: {
        'menuContent': {
          templateUrl: 'templates/appointment.html',
          controller: 'BMDCtrl'
        }
      }
    })
.state('app.my_booking', {
  cache: false,
      url: '/my_booking',
      views: {
        'menuContent': {
          templateUrl: 'templates/my_booking.html',
          controller: 'BMDCtrl'
        }
      }
    })
.state('app.search_list', {
  cache: false,
      url: '/search_list/:search_text',
      views: {
        'menuContent': {
          templateUrl: 'templates/search_listing.html',
          controller: 'BMDCtrl'
        }
      }
    })
.state('app.test', {
  cache: false,
      url: '/test',
      views: {
        'menuContent': {
          templateUrl: 'templates/test.html',
          controller: 'BMDCtrl'
        }
      }
    })
  .state('app.settings', {
    cache: false,
        url: '/settings',
        views: {
          'menuContent': {
            templateUrl: 'templates/settings.html',
            controller: 'SettingsCtrl'
          }
        }
      })
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/bmd-home');
});
